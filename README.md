# evaluation

Evaluation plots for the paper.

To run Jupyter, use the following command:

`sh ./run-docker.sh`

To stop it, run the following command:

`sh ./stop-docker.sh`
