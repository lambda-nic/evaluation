docker run --rm -v $PWD:/home/jupyteruser/work -p 8888:8888 -d --name rpy2-jupyter rpy2/rpy2:2.8.x

docker exec -u root -t -i rpy2-jupyter R -e "install.packages('ggthemes', repos='http://cran.rstudio.com/')"
docker exec -u root -t -i rpy2-jupyter R -e "install.packages('gridExtra', repos='http://cran.rstudio.com/')"

docker exec -t -i rpy2-jupyter jupyter notebook list
